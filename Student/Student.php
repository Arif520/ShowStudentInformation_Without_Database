<?php


class student
{

    protected $firstName;
    protected $lastName;
    protected $roll;
    protected $banglaMarks;
    protected $englishMarks;
    protected $mathMarks;
    protected $gender;


    public function setData($postArray){

        if(array_key_exists("FirstName",$postArray)){
            $this->firstName = $postArray["FirstName"];
        }
        if(array_key_exists("LastName",$postArray)){
            $this->lastName = $postArray["LastName"];
        }
        if(array_key_exists("roll",$postArray)){
            $this->roll = $postArray["roll"];
        }
        if(array_key_exists("banglaMarks",$postArray)){
            $this->banglaMarks = $postArray["banglaMarks"];
        }
        if(array_key_exists("englishMarks",$postArray)){
            $this->englishMarks = $postArray["englishMarks"];
        }
        if(array_key_exists("mathMarks",$postArray)){
            $this->mathMarks = $postArray["mathMarks"];
        }
        if(array_key_exists("Gender",$postArray)){
            $this->gender = $postArray["Gender"];
        }

    }// end of setData()


    public function showData(){

        $template = file_get_contents("ShowStudentTemplate.html");

        $template = str_replace("##firstName##",$this->firstName,$template);
        $template = str_replace("##lastName##",$this->lastName,$template);
        $template = str_replace("##roll##",$this->roll,$template);
        $template = str_replace("##banglaMarks##",$this->banglaMarks,$template);
        $template = str_replace("##englishMarks##",$this->englishMarks,$template);
        $template = str_replace("##mathMarks##",$this->mathMarks,$template);
        $template = str_replace("##gender##",$this->gender,$template);


        echo $template;

    }

}

