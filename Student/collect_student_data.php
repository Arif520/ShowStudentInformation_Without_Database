<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Information Collection Form</title>
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <form action="ProcessStudentData.php" method="post">
        <div class="form-group">
            <label for="FirstName">First Name</label>
            <input type="text" class="form-control" name="FirstName" placeholder="Enter First Name Here...">
        </div>

        <div class="form-group">
            <label for="LastName">Last Name</label>
            <input type="text" class="form-control" name="LastName" placeholder="Enter Last Name Here...">
        </div>
        <div class="form-group">
            <label for="roll">Roll Number</label>
            <input type="text" class="form-control" name="roll">
        </div>
        <div class="form-group">
            <label for="banglaMarks">Bangla MArks</label>
            <input type="text" class="form-control" name="banglaMarks">
        </div>
        <div class="form-group">
            <label for="englishMarks">English Marks</label>
            <input type="text" class="form-control" name="englishMarks">
        </div>
        <div class="form-group">
            <label for="mathMarks">Math Marks</label>
            <input type="text" class="form-control" name="mathMarks">
        </div>

        <div class="form-group">
            <label for="Gender">Gender</label>
            <input type="radio" id="Gender" name="Gender" value="Male"> Male
            <input type="radio"  id="Gender" name="Gender" value="Female"> Female
        </div>


        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</div>


</body>
</html>